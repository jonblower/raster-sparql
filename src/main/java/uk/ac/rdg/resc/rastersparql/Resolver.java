/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql;

import uk.ac.rdg.resc.rastersparql.grid.GridCollection;
import uk.ac.rdg.resc.rastersparql.grid.GridCell;
import uk.ac.rdg.resc.rastersparql.grid.Grid;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Maps between 
 * @author jon
 */
public final class Resolver
{
    private final String prefix;
    private final GridCollection gridColl;
    
    /** Looks for matches to "grids/${gridId}" */
    private static final String GRID_PATTERN = "grids\\/(?<gridId>.+)";
    
    /** Looks for matches to "grids/${gridId}/cells/[${i},${j}]" */
    private static final String GRID_CELL_PATTERN = GRID_PATTERN +
            "\\/cells\\/(?<cellI>\\d+)\\,(?<cellJ>\\d+)";
    
    /** Looks for matches to "grids/${gridId}/cells/[${i},${j}]/geometry" */
    private static final String GRID_CELL_GEOMETRY_PATTERN =
        GRID_CELL_PATTERN + "\\/geometry";
    
    public Resolver(String prefix, GridCollection gridColl)
    {
        this.prefix = prefix;
        this.gridColl = gridColl;
    }
    
    /** Finds the object to which the given URI resolves, or returns null if there
     * is no match. */
    public Object resolve(String uri)
    {
        // Make sure the URI comes from this namespace, otherwise we can't resolve the URI
        if (!uri.startsWith(prefix)) {
            return null;
        }
        
        // remove the prefix
        uri = uri.substring(prefix.length());
        
        // TODO: error handling in the below
        
        // We have to do the most complex example first, otherwise we'll get
        // a spurious match. TODO: tighten up the regexes to avoid this
        
        // Look for matches for grid cell geometries
        //System.out.printf("Trying to match %s against %s (geometry)", uri, GRID_CELL_GEOMETRY_PATTERN);
        Matcher matcher = Pattern.compile(GRID_CELL_GEOMETRY_PATTERN).matcher(uri);
        if (matcher.matches()) {
            //System.out.println("Matched geometry");
            String gridId = matcher.group(1);
            int i = Integer.parseInt(matcher.group(2));
            int j = Integer.parseInt(matcher.group(3));
            GridCell cell = new GridCell(gridColl.get(gridId), i, j);
            return cell.getGeometry();
        }
        
        // Look for matches for grid cells
        //System.out.printf("Trying to match %s against %s (grid cell)", uri, GRID_CELL_PATTERN);
        matcher = Pattern.compile(GRID_CELL_PATTERN).matcher(uri);
        if (matcher.matches()) {
            String gridId = matcher.group(1);
            int i = Integer.parseInt(matcher.group(2));
            int j = Integer.parseInt(matcher.group(3));
            return new GridCell(gridColl.get(gridId), i, j);
        }
        
        // Look for matches for grids
        //System.out.printf("Trying to match %s against %s (grid)", uri, GRID_PATTERN);
        matcher = Pattern.compile(GRID_PATTERN).matcher(uri);
        if (matcher.matches()) {
            return gridColl.get(matcher.group(1));
        }
        
        return null;
    }
    
    public String getUri(Grid grid) {
        return this.prefix + "grids/" + grid.getId();
    }
    
    /** Gets the URI representing the given GridCell */
    public String getUri(GridCell gridCell)
    {
        return getUri(gridCell.getGrid()) + "/cells/" + gridCell.getI() + "," + gridCell.getJ();
    }
    
    /** Gets the URI representing the geometry of the given GridCell */
    public String getGeometryUri(GridCell gridCell)
    {
        return getUri(gridCell) + "/geometry";
    }
    
    public static void main(String[] args)
    {
        String uri = "grids/grid1/cells/4,2/geometry";
        Pattern gridCell = Pattern.compile(GRID_PATTERN);
        Matcher matcher = gridCell.matcher(uri);
        int n = matcher.groupCount();
        if (matcher.matches()) {
            System.out.printf("Matched %d terms%n", n);
            for (int i = 0; i <= n; i++) {
                System.out.printf("Match %d = %s%n", i, matcher.group(i));
            }
        } else {
            System.out.println("No match to " + uri);
        }
    }
    
}
