/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.graph.TripleMatch;
import com.hp.hpl.jena.graph.impl.GraphBase;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.util.iterator.Map1;
import com.hp.hpl.jena.util.iterator.NiceIterator;
import java.util.Iterator;
import uk.ac.rdg.resc.rastersparql.vocabs.Rdf;

/**
 * Abstract class that helps with the construction of graphs.
 * @author jon
 */
public abstract class AbstractGraph extends GraphBase
{

    /**
     * This is the entry point to the class; the job of this method is to find
     * all the triples that match the given pattern. This cannot be overridden
     * in subclasses.
     */
    @Override
    protected final ExtendedIterator<Triple> graphBaseFind(TripleMatch m)
    {
        // The Javadoc for TripleMatch suggests that this interface will
        // become obsolete, so we convert to a Triple straight away
        Triple trip = m.asTriple();
        //System.out.println("Looking for triple: " + trip.toString());
        
        // There are eight possible patterns we can match. The subject, predicate
        // and object can either be a variable or not.
        // (Blank nodes act like variables so we don't need to handle them explicitly here)
        
        Node subject = trip.getSubject();
        Node predicate = trip.getPredicate();
        Node object = trip.getObject();
        
        if (subject.isURI())
        {
            if (predicate.isURI()) {
                if (object.matches(Node.ANY)) {
                    // (s, p, ?o) pattern
                    return getObjects(subject, predicate);
                } else {
                    // (s, p, o) pattern
                    return checkTriples(subject, predicate, object);
                }
            } else {
                // Predicate is a variable
                if (object.matches(Node.ANY)) {
                    // (s, ?p, ?o) pattern
                    return allTriplesForSubject(subject);
                } else {
                    // (s, ?p, o) pattern
                    return findPredicates(subject, object);
                }
            }
        }
        else
        {
            // Subject is a variable
            if (predicate.isURI()) {
                if (object.matches(Node.ANY)) {
                    // (?s, p, ?o) pattern
                    return allMatchesForPredicate(predicate);
                } else {
                    // (?s, p, o) pattern
                    // e.g. (?s rdf:type <type>)
                    return allSubjects(predicate, object);
                }
            } else {
                // Predicate is a variable
                if (object.matches(Node.ANY)) {
                    // (?s, ?p, ?o) pattern
                    return allTriples();
                } else {
                    // (?s, ?p, o) pattern
                    return matchObject(object);
                }
            }
        }
    }
    
    /** Returns an iterator over all triples in the graph.*/
    private ExtendedIterator<Triple> allTriples()
    {
        // Get an iterator over all subject URIs
        Iterator<String> subjects = allSubjects();
        
        // Create a mapping from subject URIs to iterators of triples that
        // contain the subject
        Map1<String, Iterator<Triple>> mapper = new Map1<String, Iterator<Triple>>() {

            @Override
            public Iterator<Triple> map1(String subject) {
                return allTriplesForSubject(NodeFactory.createURI(subject));
            }
            
        };
        
        // Create a single Iterator that concatenates all the iterators from
        // the subjects
        return Utils.flatMap(subjects, mapper);
    }
    
    /** Returns all triples that have the given predicate */
    private ExtendedIterator<Triple> allMatchesForPredicate(final Node predicate)
    {
        final String predicateUri = predicate.getURI();
        
        // Get all subjects that have the the given predicate
        Iterator<String> subjects = allSubjectsForPredicate(predicateUri);
        
        // Create a mapping from subject URIs to iterators of triples that
        // contain the subject and predicate
        Map1<String, Iterator<Triple>> mapper = new Map1<String, Iterator<Triple>>() {

            @Override
            public Iterator<Triple> map1(final String subject) {
                Iterator<Node> objects = getObjectNodes(subject, predicateUri);
                Map1<Node, Triple> mapper2 = new Map1<Node, Triple>() {
                    @Override
                    public Triple map1(Node o) {
                        return new Triple(
                            NodeFactory.createURI(subject),
                            predicate,
                            o
                        );
                    }
                };
                return Utils.map(objects, mapper2);
            }
            
        };
        
        return Utils.flatMap(subjects, mapper);
    }
    
    /** Returns an iterator of Triples that match the given subject and predicate.
        When this is called we have already verified that the subject and predicate
        are URIs. */
    private ExtendedIterator<Triple> getObjects(final Node uriSubject,
            final Node uriPredicate)
    {
        // Find all the Nodes that match the subject and predicate
        Iterator<Node> objects = this.getObjectNodes(uriSubject.getURI(),
                uriPredicate.getURI());
        // Wrap the iterator of objects as an ExtendedIterator
        ExtendedIterator<Node> objIterator = Utils.wrap(objects);
        
        // Create a mapper from an object node to a full triple
        Map1<Node, Triple> mapper = new Map1<Node, Triple>() {
            @Override
            public Triple map1(Node object) {
                return new Triple(uriSubject, uriPredicate, object);
            }
        };
        
        return objIterator.mapWith(mapper);
    }
    
    /**
     * Returns an iterator of Triples that match the given subject and object,
     * {@literal i.e.} we have to find the matching predicate(s).
     * @param uriSubject we know this is a URI
     * @param object we know this isn't a variable, but could be a URI or literal
     * @return iterator of Triples that match the given subject and object,
     */
    private ExtendedIterator<Triple> findPredicates(final Node uriSubject, final
            Node object)
    {
        // Find all the triples that match the given subject, then filter out
        // the ones that don't match the given object
        ExtendedIterator<Triple> triples = allTriplesForSubject(uriSubject);
        // Create a filter that accepts objects that match the given one
        Filter<Triple> filter = new Filter<Triple>() {
            @Override
            public boolean accept(Triple triple) {
                return triple.objectMatches(object);
            }
        };
        // Return all triples that pass the filter
        return triples.filterKeep(filter);
    }
    
    /**
     * Returns an iterator of Triples that match the given object
     * @todo this is inefficient as it involves iterating through all the triples
     * in the graph. Would be better to be able to identify possible subject
     * or predicate types that can be associated with the given object.
     */
    private ExtendedIterator<Triple> matchObject(final Node object)
    {
        // Find all the triples in the graph, then filter out the ones that 
        // don't contain the object
        ExtendedIterator<Triple> triples = allTriples();
        // Create a filter that accepts objects that match the given one
        Filter<Triple> filter = new Filter<Triple>() {
            @Override
            public boolean accept(Triple triple) {
                return triple.objectMatches(object);
            }
        };
        // Return all triples that pass the filter
        return triples.filterKeep(filter);
    }
    
    /** Returns an iterator of Triples that match the given s, p and o, none of
     * which are variables. */
    private ExtendedIterator<Triple> checkTriples(final Node uriSubject,
            final Node uriPredicate, final Node object)
    {
        // We get a set of triples for all objects that match the given
        // subject and predicate, then check for matches to the object
        ExtendedIterator<Triple> allObjects = this.getObjects(uriSubject, uriPredicate);
        // Create a filter that accepts objects that match the given one
        Filter<Triple> filter = new Filter<Triple>() {
            @Override
            public boolean accept(Triple triple) {
                return triple.objectMatches(object);
            }
        };
        // Return all triples that pass the filter
        return allObjects.filterKeep(filter);
    }
    
    /** Returns all the triples that match the given subject, which we know is a URI */
    private ExtendedIterator<Triple> allTriplesForSubject(final Node uriSubject)
    {
        // Start with an empty iterator
        ExtendedIterator<Triple> it = Utils.emptyIterator();
        
        // Look through all the possible predicates for the subject and find
        // all possible objects for each (s,p) pair
        Iterator<String> predicates = allPredicatesForSubject(uriSubject.getURI());
        while(predicates.hasNext())
        {
            ExtendedIterator<Triple> triples = getObjects(uriSubject,
                    NodeFactory.createURI(predicates.next()));
            // Append the new iterator to the end of the existing one
            it = it.andThen(triples);
        }
        return it;
    }
    
    /** Returns all subjects that have the given predicate and object */
    private ExtendedIterator<Triple> allSubjects(Node predicate, final Node object)
    {
        // There is a common case where the predicate is rdf:type
        if (predicate.matches(NodeFactory.createURI(Rdf.TYPE))) {
            String typeUri = object.getURI();
            Iterator<String> subjects = allSubjectsOfType(typeUri);
            return getTriples(subjects, predicate, object);
        }
        
        // Else we find all triples that match the predicate and filter out
        // those that do not contain the given object
        ExtendedIterator<Triple> triples = allMatchesForPredicate(predicate);
        // Create a filter that accepts objects that match the given one
        Filter<Triple> filter = new Filter<Triple>() {
            @Override
            public boolean accept(Triple triple) {
                return triple.objectMatches(object);
            }
        };
        return triples.filterKeep(filter);
    }
    
    private ExtendedIterator<Triple> getTriples(final Iterator<String> subjects,
            final Node predicate, final Node object)
    {
        return new NiceIterator<Triple>()
        {
            @Override public boolean hasNext() { return subjects.hasNext(); }
            @Override public Triple next() {
                return new Triple(
                    NodeFactory.createURI(subjects.next()),
                    predicate,
                    object
                );
            }
        };
    }
    
    ////////////// ABSTRACT METHODS ///////////////////
    
    
    /** Returns a collection of strings containing all possible subject URIs
     * in the graph */
    protected abstract Iterator<String> allSubjects();
    
    /** Returns a collection of strings containing all possible subject URIs
     * in the graph that are of the given type */
    protected abstract Iterator<String> allSubjectsOfType(String typeUri);
    
    /** Returns a collection of strings containing all possible subject URIs
     * in the graph that can contain the given predicate URI */
    protected abstract Iterator<String> allSubjectsForPredicate(String predicateURI);
    
    /** Returns a set of all the possible predicates (URIs) that can exist in the graph */
    protected abstract Iterator<String> allPredicates();
    
    /** Returns a set of all the possible predicates (URIs) that the subject can have */
    protected abstract Iterator<String> allPredicatesForSubject(String subjUri);
    
    /** Returns a collection of Nodes that match the given subject and predicate.
     *  These are Nodes because they can either be literals or URIs. */
    protected abstract Iterator<Node> getObjectNodes(String subjUri, String predUri);
}
