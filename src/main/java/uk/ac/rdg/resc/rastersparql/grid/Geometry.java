/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.grid;

/**
 * Represents a Geometry object
 * @author jon
 */
public class Geometry {
    
    private final double lon;
    private final double lat;
    
    public Geometry(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }
    
    /** Returns this geometry as WKT */
    public String asWKT() {
        // TODO: this should be CRS:84 as below but Sextant doesn't display correctly
        return String.format("<http://www.opengis.net/def/crs/EPSG/0/4326> POINT(%f %f)", lon, lat);
        //return String.format("<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(%f %f)", lon, lat);
    }
    
}
