/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.vocabs;

/**
 * Terms from the RDF vocabulary. This class only contains final variables
 * and is non-instantiable.
 * @author jon
 */
public final class Rdf
{
    
    /** RDF's base URI */
    public static final String PREFIX = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    
    /** RDF's type predicate */
    public static final String TYPE = PREFIX + "type";
    
    
    // Default constructor throws an exception to enforce non-instantiability
    public Rdf() { throw new IllegalStateException(); }
    
    
}
