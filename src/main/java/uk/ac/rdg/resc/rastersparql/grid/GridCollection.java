/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.grid;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple collection of Grid objects, mapped by their IDs
 * @author jon
 */
public final class GridCollection {
    
    private final Map<String, Grid> grids = new HashMap<>();
    
    public void add(Grid grid) {
        this.grids.put(grid.getId(), grid);
    }
    
    public Grid get(String id) {
        return this.grids.get(id);
    }
    
}
