package uk.ac.rdg.resc.rastersparql;

import uk.ac.rdg.resc.rastersparql.grid.GridCollection;
import uk.ac.rdg.resc.rastersparql.grid.Grid;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.impl.ModelCom;
import com.hp.hpl.jena.sparql.core.DatasetImpl;
import com.hp.hpl.jena.sparql.function.FunctionRegistry;
import uk.ac.rdg.resc.rastersparql.vocabs.GeoSparql;
import uk.ac.rdg.resc.rastersparql.vocabs.Grids;

/**
 * Entry point for simple testing
 */
public class App 
{
    private static final String DEFAULT_PREFIX = "http://www.example.com/";
    
    private static final String QUERY = 
        "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
        "PREFIX exf:   <http://example.org/func#>\n" +
        "SELECT ?name ?mbox\n" +
        "WHERE {\n" +
        "    OPTIONAL {?person foaf:mbox ?mbox .}\n" +
        "    ?person foaf:name ?name .\n" +
        "    FILTER (exf:startsWith(?name, \"Jon\"))\n" +
        "}";
    
    private static final String QUERY_GRID_CELLS = 
        "PREFIX grid:  <" + Grids.PREFIX + ">\n" +
        "PREFIX geo:   <" + GeoSparql.PREFIX + ">\n" +
        "BASE          <" + DEFAULT_PREFIX + ">\n" +
        "SELECT ?wkt \n" +
        "WHERE\n" + 
        "{\n" +
        "  GRAPH <grids/grid1> {\n" +
        "      </grids/grid1/cells/0,0/geometry> geo:asWKT ?wkt .\n" +
        //"      ?s geo:hasGeometry ?geom .\n" +
        //"      OPTIONAL {?s     geo:asWKT ?wkt .}\n" +
        "  }\n" +
        "}";
    
    public static void main( String[] args )
    {
        // Create the grids that will form part of the dataset
        GridCollection coll = new GridCollection();
        Grid grid1 = new Grid("grid1", 5, 3);
        Grid grid2 = new Grid("grid2", 6, 4);
        coll.add(grid1);
        coll.add(grid2);
        
        // Create a Resolver that will map between data objects and URIs,
        // using the given URL prefix
        Resolver resolver = new Resolver(DEFAULT_PREFIX, coll);
        
        // Create a graph and model for each grid
        Model model1 = new ModelCom(new RasterGraph(resolver, grid1));
        Model model2 = new ModelCom(new RasterGraph(resolver, grid2));
        
        // Create a Dataset whose default graph is one that represents the 
        // collection of grids
        Model defModel = new ModelCom(new GridCollectionGraph(coll));
        Dataset dataset = new DatasetImpl(defModel);
        // Add a named graph for each grid
        dataset.addNamedModel(resolver.getUri(grid1), model1);
        dataset.addNamedModel(resolver.getUri(grid2), model2);
        
        FunctionRegistry.get().put("http://example.org/func#startsWith", StartsWithFunction.class) ;
        
        Query query = QueryFactory.create(QUERY_GRID_CELLS);
        
        // Execute a query against the dataset
        QueryExecution ex = QueryExecutionFactory.create(query, dataset);
        ResultSet results = ex.execSelect();
        
        Utils.printResults(System.out, results);
        
    }
}
