/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.grid;

import java.util.Iterator;

/**
 * Represents a Grid of data
 * @author jon
 */
public final class Grid implements Iterable<GridCell>
{
    private final String id;
    private final int width;
    private final int height;
    
    public Grid(String id, int width, int height)
    {
        this.id = id;
        this.width = width;
        this.height = height;
    }
    
    /** Get the id of the grid, unique within its collection */
    public String getId() { return this.id; }
    
    public int getWidth() { return this.width; }
    
    public int getHeight() { return this.height; }

    @Override
    public Iterator<GridCell> iterator() {
        return new GridCellIterator();
    }
    
    private final class GridCellIterator implements Iterator<GridCell>
    {
        private int i = 0;
        private int j = 0;
        
        @Override
        public boolean hasNext() {
            return j < height && i < width;
        }

        @Override
        public GridCell next() {
            if (hasNext()) {
                GridCell cell = new GridCell(Grid.this, i, j);// Increment the indices
                i++;
                if (i >= width) {
                    i = 0;
                    j++;
                }
                return cell;
            }
            throw new IndexOutOfBoundsException();
        }
    }
    
}
