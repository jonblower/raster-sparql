/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.server;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import uk.ac.rdg.resc.rastersparql.Utils;
import uk.ac.rdg.resc.rastersparql.vocabs.GeoSparql;
import uk.ac.rdg.resc.rastersparql.vocabs.Grids;

/**
 *
 * @author jon
 */
public class Client {
    
    private static final String QUERY_GRID_CELLS = 
        "PREFIX grid:  <" + Grids.PREFIX + ">\n" +
        "PREFIX geo:   <" + GeoSparql.PREFIX + ">\n" +
        "BASE          <" + Server.DEFAULT_PREFIX + ">\n" +
        "SELECT ?w\n" +
        "WHERE\n" + 
        "{\n" +
        "  GRAPH <grids/grid1> {\n" +
        "      </grids/grid1/cells/0,0> geo:hasGeometry ?geom .\n" +
        "      ?geom geo:asWKT ?w .\n" +
        //"      ?s geo:hasGeometry ?geom .\n" +
        //"      OPTIONAL {?s     geo:asWKT ?wkt .}\n" +
        "  }\n" +
        "}";
    
    public static void main(String[] args)
    {
        System.out.println(QUERY_GRID_CELLS);
        QueryExecution ex = QueryExecutionFactory.sparqlService(
                "http://localhost:" + Server.PORT + "/" + Server.DATASET_ID + "/query",
                QUERY_GRID_CELLS
        );
        ResultSet results = ex.execSelect();
        
        Utils.printResults(System.out, results);
    }
    
}
