/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.vocabs;

/**
 * Terms from the GeoSPARQL vocabulary. This class only contains final variables
 * and is non-instantiable.
 * @author jon
 */
public final class GeoSparql
{
    
    /** GeoSPARQL's base URI */
    public static final String PREFIX = "http://www.opengis.net/ont/geosparql#";
    
    /** GeoSPARQL's hasGeometry predicate */
    public static final String HAS_GEOMETRY = PREFIX + "hasGeometry";
    
    /** GeoSPARQL's asWKT predicate */
    public static final String AS_WKT = PREFIX + "asWKT";
    
    /** GeoSPARQL's WKT literal data type */
    public static final String WKT_LITERAL = PREFIX + "wktLiteral";
    
    
    // Default constructor throws an exception to enforce non-instantiability
    public GeoSparql() { throw new IllegalStateException(); }
    
}
