/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.util.iterator.Map1;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import uk.ac.rdg.resc.rastersparql.grid.Geometry;
import uk.ac.rdg.resc.rastersparql.grid.Grid;
import uk.ac.rdg.resc.rastersparql.grid.GridCell;
import uk.ac.rdg.resc.rastersparql.vocabs.GeoSparql;
import uk.ac.rdg.resc.rastersparql.vocabs.Grids;
import uk.ac.rdg.resc.rastersparql.vocabs.Rdf;

/**
 * A synthetic RDF graph representing a raster image
 * @author jon
 */
public class RasterGraph extends AbstractGraph {
    
    private final Resolver resolver;
    private final Grid grid;
    
    /** A mapper from grid cells to their URIs */
    private final Map1<GridCell, String> gridCellToUriMapper = new Map1<GridCell, String>() {
        @Override
        public String map1(GridCell o) {
            return resolver.getUri(o);
        }
    };
    
    /** A mapper from grid cells to the URI of their geometries */
    private final Map1<GridCell, String> gridCellToGeometryUriMapper = new Map1<GridCell, String>() {
        @Override
        public String map1(GridCell o) {
            return resolver.getGeometryUri(o);
        }
    };
    
    public RasterGraph(Resolver resolver, Grid grid)
    {
        this.resolver = resolver;
        this.grid = grid;
    }
    
    private Iterator<String> allGridCellUris() {
        return Utils.map(grid.iterator(), gridCellToUriMapper);
    }
    
    private Iterator<String> allGridCellGeometryUris() {
        return Utils.map(grid.iterator(), gridCellToGeometryUriMapper);
    }

    @Override
    protected Iterator<String> allSubjects() {
        // Subjects can be grid cells or geometries
        return Utils.andThen(allGridCellUris(), allGridCellGeometryUris());
    }

    @Override
    protected Iterator<String> allSubjectsForPredicate(String predicateURI) {
        if (GeoSparql.HAS_GEOMETRY.equals(predicateURI)) {
            // We need to find all subjects that can have geometries. At the
            // moment only grid cells can have geometries
            return allGridCellUris();
        }
        if (Rdf.TYPE.equals(predicateURI)) {
            // We need to find all subjects that have a type, which means all
            // subjects!
            return this.allSubjects();
        }
        if (GeoSparql.AS_WKT.equals(predicateURI)) {
            // We need to find everything that can have a geo:asWKT predicate
            // This means all the geometries, which can only belong to grid
            // cells
            return allGridCellGeometryUris();
        }
        return Collections.emptyIterator();
    }

    @Override
    protected Iterator<String> allPredicates() {
        Set<String> predicates = new HashSet<>();
        predicates.add(Rdf.TYPE);
        predicates.add(GeoSparql.HAS_GEOMETRY);
        predicates.add(GeoSparql.AS_WKT);
        return predicates.iterator();
    }

    @Override
    protected Iterator<String> allPredicatesForSubject(String subjUri) {
        // Find out what kind of subject this is
        Object obj = resolver.resolve(subjUri);
        if (obj instanceof GridCell)
        {
            Set<String> predicates = new HashSet<>();
            predicates.add(Rdf.TYPE);
            predicates.add(GeoSparql.HAS_GEOMETRY);
            // TODO: add predicates representing the values the grid cell can take
            return predicates.iterator();
        }
        else if (obj instanceof Geometry)
        {
            Set<String> predicates = new HashSet<>();
            predicates.add(Rdf.TYPE);
            predicates.add(GeoSparql.AS_WKT);
            return predicates.iterator();
        }
        // If we've got this far there is no match
        return Collections.emptyIterator();
    }

    @Override
    protected Iterator<Node> getObjectNodes(String subjUri, String predUri) {
        // Find out what kind of subject this is
        Object subj = resolver.resolve(subjUri);
        if (subj instanceof GridCell) {
            GridCell gridCell = (GridCell)subj;
            if (Rdf.TYPE.equals(predUri)) {
                Set<Node> objects = new HashSet<>();
                objects.add(NodeFactory.createURI(Grids.GRID_CELL));
                return objects.iterator();
            } else if (GeoSparql.HAS_GEOMETRY.equals(predUri)) {
                Set<Node> objects = new HashSet<>();
                objects.add(NodeFactory.createURI(resolver.getGeometryUri(gridCell)));
                return objects.iterator();
            }
        } else if (subj instanceof Geometry) {
            Geometry geom = (Geometry)subj;
            if (GeoSparql.AS_WKT.equals(predUri)) {
                Set<Node> objects = new HashSet<>();
                objects.add(NodeFactory.createLiteral(geom.asWKT(), new WktDataType()));
                return objects.iterator();
            }
            if (Rdf.TYPE.equals(predUri)) {
                Set<Node> objects = new HashSet<>();
                // TODO: do this properly
                objects.add(NodeFactory.createURI(GeoSparql.PREFIX + "Geometry"));
                return objects.iterator();
            }
        }
        
        return Collections.emptyIterator();
    }

    @Override
    protected Iterator<String> allSubjectsOfType(String typeUri) {
        if (Grids.GRID_CELL.equals(typeUri)) {
            // Return all grid cells
            return allGridCellUris();
        } else if (typeUri.equals(GeoSparql.PREFIX + "Geometry")) {
            // TODO: check for subtypes of geometry
            return allGridCellGeometryUris();
        }
        return Collections.emptyIterator();
    }
}
