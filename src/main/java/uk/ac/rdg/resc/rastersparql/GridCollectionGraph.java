/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql;

import uk.ac.rdg.resc.rastersparql.grid.GridCollection;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.graph.TripleMatch;
import com.hp.hpl.jena.graph.impl.GraphBase;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.util.iterator.NiceIterator;

/**
 * Graph representing a collection of grids.
 * @todo this doesn't actually work yet!
 * @author jon
 */
public class GridCollectionGraph extends GraphBase
{
    
    private final GridCollection coll;
    
    public GridCollectionGraph(GridCollection coll)
    {
        this.coll = coll;
    }
    
    /**
     * Returns an iterator over all triples that match the given pattern.
     */
    @Override
    protected ExtendedIterator<Triple> graphBaseFind(TripleMatch m)
    {
        // The Javadoc for TripleMatch suggests that this interface will
        // become obsolete, so we convert to a Triple straight away
        Triple trip = m.asTriple();
        
        System.out.println("Collection: looking for triple: " + trip.toString());
        
        // Nothing matches against the query so we return an empty iterator
        System.out.println("No matches in graph");
        return new NiceIterator<Triple>();
    }
    
}
