/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql;

import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.function.FunctionBase2;

/**
 * Function that tests to see if one string starts with another
 * @author jon
 */
public class StartsWithFunction extends FunctionBase2{

    @Override
    public NodeValue exec(NodeValue v1, NodeValue v2) {
        System.out.printf("Calling startsWith(%s, %s) = ", v1, v2);
        // TODO: this doesn't work properly. How do we get the string values of v1 and v2?
        NodeValue result = "Jon Blower".startsWith("Jon")
                ? NodeValue.TRUE
                : NodeValue.FALSE ;
        System.out.println(result);
        return result;
    }
    
}
