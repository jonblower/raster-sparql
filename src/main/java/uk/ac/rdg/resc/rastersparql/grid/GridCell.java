/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.grid;

/**
 * Represents a single cell within a {@link Grid}
 * @author jon
 */
public final class GridCell
{
    private final int i;
    private final int j;
    private final Grid grid;
    
    public GridCell(Grid grid, int i, int j)
    {
        this.grid = grid;
        this.i = i;
        this.j = j;
    }
    
    public int getI() { return this.i; }
    
    public int getJ() { return this.j; }
    
    public Grid getGrid() { return this.grid; }
    
    public Geometry getGeometry()
    {
        return new Geometry(i, j) {};
    }
}
