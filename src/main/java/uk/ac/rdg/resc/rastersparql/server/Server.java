/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.server;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.impl.ModelCom;
import com.hp.hpl.jena.sparql.core.DatasetImpl;
import org.apache.jena.fuseki.EmbeddedFusekiServer;
import uk.ac.rdg.resc.rastersparql.GridCollectionGraph;
import uk.ac.rdg.resc.rastersparql.RasterGraph;
import uk.ac.rdg.resc.rastersparql.Resolver;
import uk.ac.rdg.resc.rastersparql.grid.Grid;
import uk.ac.rdg.resc.rastersparql.grid.GridCollection;

/**
 * Uses Fuseki to run a server hosting a graph
 * @author jon
 */
public class Server {
    
    public static final String DEFAULT_PREFIX = "http://www.example.com/";
    public static final int PORT = 8088;
    public static final String DATASET_ID = "raster";
    
    public static void main(String[] args) {
        
        // Create the grids that will form part of the dataset
        GridCollection coll = new GridCollection();
        Grid grid1 = new Grid("grid1", 5, 3);
        Grid grid2 = new Grid("grid2", 6, 4);
        coll.add(grid1);
        coll.add(grid2);
        
        // Create a Resolver that will map between data objects and URIs,
        // using the given URL prefix
        Resolver resolver = new Resolver(DEFAULT_PREFIX, coll);
        
        // Create a graph and model for each grid
        Model model1 = new ModelCom(new RasterGraph(resolver, grid1));
        Model model2 = new ModelCom(new RasterGraph(resolver, grid2));
        
        // Create a Dataset whose default graph is one that represents the 
        // collection of grids
        Model defModel = new ModelCom(new GridCollectionGraph(coll));
        Dataset dataset = new DatasetImpl(defModel);
        // Add a named graph for each grid
        dataset.addNamedModel(resolver.getUri(grid1), model1);
        dataset.addNamedModel(resolver.getUri(grid2), model2);
        
        EmbeddedFusekiServer.create(PORT, dataset.asDatasetGraph(), "raster").start();
    }
    
}
