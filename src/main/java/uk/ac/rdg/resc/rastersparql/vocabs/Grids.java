/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.rdg.resc.rastersparql.vocabs;

/**
 * Terms from the Grids vocabulary. This class only contains final variables
 * and is non-instantiable.
 * @author jon
 */
public final class Grids
{
    
    /** Base URI for Grids vocabulary */
    public static final String PREFIX = "http://www.example.org/grids/ont#";
    
    /** URI representing the type of a Grid */
    public static final String GRID = PREFIX + "Grid";
    
    /** URI representing the type of a GridCell */
    public static final String GRID_CELL = PREFIX + "GridCell";
    
    
    // Default constructor throws an exception to enforce non-instantiability
    public Grids() { throw new IllegalStateException(); }
    
    
}
