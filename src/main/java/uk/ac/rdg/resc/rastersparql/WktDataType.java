package uk.ac.rdg.resc.rastersparql;


import com.hp.hpl.jena.datatypes.BaseDatatype;
import uk.ac.rdg.resc.rastersparql.vocabs.GeoSparql;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jon
 */
public class WktDataType extends BaseDatatype {

    public WktDataType() {
        super(GeoSparql.WKT_LITERAL);
    }
    
    
    
}
